var express = require('express');
var router = express.Router();
var app_controller = require('../controllers/app_controller');

router.get("/", app_controller.renderIndex);
router.get("/analizador", app_controller.renderAnalyzer);
router.post("/analizador", app_controller.analyze);
router.get("/visorhtml", app_controller.renderViewer);
router.post("/visorhtml", app_controller.view);
module.exports = router;