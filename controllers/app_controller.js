var fs = require('fs');
var lineReader = require('line-reader');
var patterns = [
	{pattern: new RegExp("%!PS-Adobe-3.0 EPSF-3.0"), name: "ID_COMMENT", cssclass: "id_comment"},
	{pattern: new RegExp("%!PS-Adobe-2.0 EPSF-1.2"), name: "ID_COMMENT", cssclass: "id_comment"},
	{pattern: new RegExp("%!PS"), name: "ID_COMMENT", cssclass: "id_comment"},
	{pattern: new RegExp(/^%%EOF$/), name: "ENDOFFILE", cssclass: "eof"},
	{pattern: new RegExp(/\%.*/), name: "COMMENT", cssclass: "cmm"},
	{pattern: new RegExp(/\(([^)]+)|\(/), name: "LEFTPARENTHESES", cssclass: "left_par"},
    {pattern: new RegExp(/\)/), name: "RIGHTPARENTHESES", cssclass: "right_par"},
    {pattern: new RegExp(/\(([^)]+)\)$/), name: "STRING", cssclass: "str"},
    {pattern: new RegExp(/^setlinewidth$|^makefont$|^restore$|^rmoveto$|^rlineto$|^arc$|^arcn$|^arct$|^arcto$|^rcurveto$|^closepath$|^fill$|^newpath$|^setrgbcolor$|^curveto$|^scale$|^scalefont$|^setfont$|^moveto$|^lineto$|^stroke$|^findfont$|^show$|^showpage$|^def$|^where$|^ifelse$|^save$|^grestore$|^if$|^gsave$|^setcmykcolor$|^for$|^repeat$|^translate$|^rotate$/), name: "RESERVEDWORD", cssclass: "res_word"},
    {pattern: new RegExp(/^add$|^sub$|^mul$|^div$|^idiv$|^mod$|^abs$|^neg$|^ceiling$|^floor$|^round$|^truncate$|^sqrt$|^exp$|^ln$|^log$^sin$|^cos$|^atan$|^rand$|^srand$|^rrand$/), name: "OPERATORS", cssclass:"art_ope"},
    {pattern: new RegExp(/^\{$/), name: "LEFT-BRACE", cssclass: "lf_brc"},
 	{pattern: new RegExp(/^\}$/), name: "RIGHT-BRACE", cssclass: "rg_brc"},
 	{pattern: new RegExp(/^\[$/), name: "LEFT-BRACKET", cssclass: "lf_brckt"},
 	{pattern: new RegExp(/^\]$/), name: "RIGHT-BRACKET", cssclass: "rg_brckt"},
 	{pattern: new RegExp(/^[\s]*[\-]?[0-9]+$/), name: "NUMBER", cssclass: "num_int"},
 	{pattern: new RegExp(/[\-]?[0-9]*\.[0-9]+$/), name: "FLOAT", cssclass: "num_float"},
    {pattern: new RegExp(/[a-zA-Z0-9\-]+/), name: "VARIABLE", cssclass: "var"},
    {pattern: new RegExp(/\/[a-zA-Z0-9\-]+/), name: "VARIABLE", cssclass: "var"},
    {pattern: new RegExp(/[\/\+\-\=*]/), name: "OPERATOR", cssclass: "ope"},
    {pattern: new RegExp(/./), name: "NOT RECOGNIZE", cssclass: "not_recognize"},
]

var output;
var original;
var template1 = '<html><head><meta http-equiv="content-type" content="text/html; charset=UTF-8"/><meta name="description" content="PostScript Lexer LP-UNAL"/><meta name="keywords" content="" /><meta name="author" content="SEBASTIÁN ALARCÓN" /><link rel="stylesheet" type="text/css" href="style.css" media="screen" /><title>PostScript Lexer LP-UNAL</title></head><body><div class="main-wrapper"><div class="result default">';
var template2 = '</div></div></body></html>';


//testig files!
// var filename = './uploads/0_LenguajesUN.ps'
// var filename = './uploads/FractalArrows.ps'
// var filename = './uploads/PrintColorNames.ps'
// var filename = './uploads/RandomButterflies.ps'
// var filename = './uploads/RandomFlowers1.ps'
// var filename = './uploads/RandomFlowers2.ps'
// var filename = './uploads/TwoConectedDashedArcs.ps'
// var filename = './uploads/TwoConectedWaveArcs.ps'
// var filename = './uploads/alphabet.ps'
// var filename = './uploads/box_fractal.ps'
// var filename = './uploads/butterfly.ps'
// var filename = './uploads/cells.ps'
// var filename = './uploads/chess.ps'
// var filename = './uploads/circle.ps'
// var filename = './uploads/cmyk_color_test.ps'
// var filename = './uploads/color_escher.ps'
// var filename = './uploads/colorcir.ps'
// var filename = './uploads/def.ps'
// var filename = './uploads/doretree.ps'
// var filename = './uploads/escher.ps'
// var filename = './uploads/eye.ps'
// var filename = './uploads/fact.ps'
// var filename = './uploads/flowers_1.ps'
// var filename = './uploads/flowers_2.ps'
// var filename = './uploads/for.ps'
// var filename = './uploads/fractals.ps'
// var filename = './uploads/gaussian.ps'
// var filename = './uploads/golfer.ps'
// var filename = './uploads/grayalph.ps'
// var filename = './uploads/grid.ps'
// var filename = './uploads/hilbert_curves.ps'
// var filename = './uploads/hsb_color_test.ps'
// var filename = './uploads/koch.ps'
// var filename = './uploads/koch_snowflake.ps'
// var filename = './uploads/multiflake.ps'
// var filename = './uploads/new.ps'
// var filename = './uploads/nozzle.ps'
// var filename = './uploads/parrot.ps'
// var filename = './uploads/peter.ps'
// var filename = './uploads/photon.ps'
// var filename = './uploads/porsche.ps'
// var filename = './uploads/rgb_color_test.ps'
// var filename = './uploads/roseart.ps'
// var filename = './uploads/sample.ps'
// var filename = './uploads/sierpinski.ps'
// var filename = './uploads/snowflak.ps'
// var filename = './uploads/starchart.ps'
// var filename = './uploads/step.ps'
// var filename = './uploads/test.ps'
// var filename = './uploads/text.ps'
// var filename = './uploads/texto.ps'
// var filename = './uploads/tiger.ps'
// var filename = './uploads/transparency_example.ps'
// var filename = './uploads/vasarely.ps'
// var filename = './uploads/waterfal.ps'



exports.renderIndex = function(req, res, next) {
	res.render('index', { title: 'PostScript', subtitle: "Analizador Léxico"});
}

exports.renderAnalyzer = function(req, res, next) {
	res.render('lexanalyzer', { title: 'PostScript', subtitle: "Analizador Léxico"});
}


lex_analyzer = function(line){
	original+=line
	var words = line.split(" ");
	token_comment = false;
	token_string = false;
	token_eof = false;
	toke_desc_file = false;
	for(var i=0 ; i<words.length; i++){
		//check if the line is the description file
		if(!toke_desc_file){
			m=patterns[0].pattern.exec(words[i])
			n=patterns[1].pattern.exec(words[i])
			o=patterns[2].pattern.exec(words[i]) 
			if(m||n||o){
				token_desc_file = true
				output+="<span class='id_comment'>"+words[i]+"</span>"
				continue;
			}
		}

		//check if the line is the end of file
		if(!token_eof){
			m=patterns[3].pattern.exec(words[i])
			if(m){
				token_eof = true
				output+="<span class='eof'>"+words[i]+"</span>"
				continue;
			}
		}

		//check if the line has a comment
		if(!token_comment && !token_string){
			re = patterns[4].pattern;
			m=re.exec(words[i])
			if(m){
				token_comment = true
				output+="<span class='cmm'>"+m[0]
				if(i==(words.length-1)){
					output+="</span>";
				}else{
					output+=" "
				}
				continue;
			}
		}else if(token_comment){
			if(i==words.length-1)
				output+=words[i]+"</span>";
			else
				output+=words[i]
			output+=" "
			continue;
		}

		//check if there is a String ()
		if(!token_string ){
			re = patterns[7].pattern;
			m=re.exec(words[i])
			if(m){
				token_string = false
				output+="<span class='str'>"+words[i]+"</span>"
				output+=" "
				continue;
			}else{
				re = patterns[5].pattern;
				m=re.exec(words[i])
				if(m){
					token_string = true
					output+="<span class='str'>"+m[0]
					output+=" "
					continue;
				}
			}
		}else if(token_string){
			re = patterns[6].pattern;
			m=re.exec(words[i])
			if(m){
				token_string = false;
				output+=words[i]+"</span>";
			}else{
				output+=words[i]
			}
			output+=" "
			continue;
		}

		if(!token_string && !token_comment){
			for(j=0;j<patterns.length;j++){
				if(patterns[j].pattern.test(words[i].trim())){
					output += '<span class="'+patterns[j].cssclass+'">'+words[i]+'</span>';
					output+=" ";
					break;
				}
			}
		}
	}
}


exports.analyze = function(req,res,next){
	output = ""
	original = ""
	if(req.file !== undefined){
		//If the user upload a file
		filename = './uploads/'+req.file.filename
		lineReader.eachLine(filename, function(line, last, cb) {
			lex_analyzer(line)
			if (last ) {
				fs.writeFile("./generated_files/"+req.file.filename+'.html', template1+output+template2, function (err) {
					if (err) throw err;
					console.log('It\'s saved!');
				});
				res.render("output", {title: 'PostScript', subtitle: "Resultado de Análisis", original: original, output: output, file_download:req.file.filename+'.html'})
				cb(false); // stop reading
			} else {
				output += "<br>"
				original += "<br>"
				cb();
			}
		});
	}else{
		//If the user set a text in the textarea
		lines = req.body.code.split(/\n/)
		for(var i=0; i<lines.length;i++){
			lex_analyzer(lines[i])
			output += "<br>"
			original += "<br>"
		}

		name = Math.random().toString(36).substring(7)
		fs.writeFile("./generated_files/"+name+'.html', template1+output+template2, function (err) {
					if (err) throw err;
					console.log('It\'s saved!');
				});
		res.render("output", {title: 'PostScript', subtitle: "Resultado de Análisis", original: original, output: output, file_download:name+'.html'})
	}
}

exports.renderViewer = function(req,res,next){
	res.render('viewer', { title: 'PostScript', subtitle: "Visor HTML"});
}

exports.view = function(req,res,next){
	if(req.file !== undefined){
		//If the user upload a file
		filename = './uploads/'+req.file.filename
		lineReader.eachLine(filename, function(line, last, cb) {
			initialIndex=line.indexOf("<span")
			lastIndex=line.lastIndexOf("</span>") + 7
			output = line.substring(initialIndex, lastIndex)
			if (last ) {
				res.render("vieweroutput", {title: 'PostScript', subtitle: "Visor HTML", output: output})
				cb(false); // stop reading
			} else {
				cb();
			}
		});
	}
}