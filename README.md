# LÉEME #

Este es el proyecto 1 de la asignatura Lenguajes de Programación 2015-2. 

El proyecto consiste en un analizador léxico para el lenguaje PostScript.



## ¿Para qué es este repositorio? ##

* Alojar el código escrito por el autor del mismo
* Alojar los ejemplos con los cuales puede ser usada la herramienta
* Instrucciones de instalación y uso

## ¿Como hago la instalación? ##

### Resumen ###

La aplicación está desarrollada en lenguaje de servidor NodeJS y en el front tiene componentes de JavaScript, JQuery, HTML y CSS.

En resumen, para usar la aplicación se debe tener instalado nodejs en su ordenador, así como el administrador de paquetes npm (Node Package Manager). A continuación se indicará paso a paso cómo debe ser la configuración y ejecución de la aplicación.

### Clonar el repositorio ###
Lo primero que debe hacer es clonar el repositorio, para ello introduzca el siguiente texto en una terminal:

```
#!git_commands

git clone https://jsalarconr@bitbucket.org/jsalarconr/postscriptlexanalyzer.git
```

### Instalación de Node ###
Si usted está bajo una distribución Linux como Ubuntu, basta con ejecutar los siguientes comandos para instalar Node en su ordenador:

```
#!linux_commands

sudo apt-get update
sudo apt-get install nodejs
```
Acto seguido, deberá instalar npm para instalar los paquetes necesarios para ejecutar la aplicación:

```
#!linux_commands

sudo apt-get install npm
```
Para ver una guía más avanzada puede hacer click [aquí](https://www.digitalocean.com/community/tutorials/how-to-install-node-js-on-an-ubuntu-14-04-server)


### Dependencias ###
Una vez instalado *node* y *npm*, deberá instalar todos los paquetes necesarios para ejecutar el proyecto. Para ello, vaya a la carpeta clonada del repositorio y escriba:

```
#!linux_commands

sudo npm install
```
Espere a que termine el proceso y estará listo para ejecutar el proyecto.

### Ejecutar el proyecto ###
Una vez instalados los paquetes necesarios, estando situado en la carpeta clonada del repositorio, escriba:

```
#!linux_commands

node bin/www
```
El servidor se estará ejecutando en este momento. Acceda a la url: localhost:3000 en un navegador para ver la aplicación corriendo.

## Guía de funcionamiento ##

* Al acceder a la aplicación a través del navegador, encontrará una página de inicio. Allí se verán las principales características del programa
* Para analizar código en PostScript, haga click en el link "Analizador Léxico". Allí tendrá un selector para subir un archivo con la extensión .ps o escribir código en dicho lenguaje.
* Una vez analizado el código, se generará una salida en HTML con el código resaltado. Podrá cambiar el esquema de colores con la lista desplegable que se encuentra allí.
* Si quiere visualizar un archivo HTML que haya sido generado con la herramienta, haga click en "Revisar un archivo en HTML". Allí subirá un archivo con la extensión .html y se generará una salida con el archivo que subió y el texto resaltado. Aquí también podrá los esquemas de color con los que podrá visualizar el código.
* Bajo la carpeta "generated_files" se guardan los archivos generados por el programa, así como la hoja de estilos en la que están las clases para poder visualizar el código resaltado.
* Bajo la carpeta "uploads" se encuentran los archivos en .ps para poder ser usados en la aplicación

## ¿Con quién contactar? ##

* Sebastián Alarcón. jsalarconr@unal.edu.co
